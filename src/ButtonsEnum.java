public enum ButtonsEnum {
    INPUT_FIELD("input"),
    TWO_ND_BUTTON("Btn2nd"),
    PI_BUTTON("BtnPi"),
    E_BUTTON("BtnE"),
    MATRIX_BUTTON("BtnMatrix"),
    VAR_BUTTON("BtnVar"),
    PARAN_L_BUTTON("BtnParanL"),
    COLON_BUTTON("BtnColon"),
    PARAN_R_BUTTON("BtnParanR"),
    LEFT_BUTTON("BtnLeft"),
    BACK_BUTTON("BtnBack"),
    SIN_BUTTON("BtnSin"),
    SIN_H_BUTTON("BtnSinH"),
    COT_BUTTON("BtnCot"),
    SQRT_Y_BUTTON("BtnSqrtY"),
    POW_XY_BUTTON("BtnPowXY"),
    SEVEN_BUTTON("Btn7"),
    EIGHT_BUTTON("Btn8"),
    NINE_BUTTON("Btn9"),
    DIV_BUTTON("BtnDiv"),
    CLEAR_BUTTON("BtnClear"),
    COS_BUTTON("BtnCos"),
    COS_H_BUTTON("BtnCosH"),
    SEC_BUTTON("BtnSec"),
    SQRT_3_BUTTON("BtnSqrt3"),
    POW_3_BUTTON("BtnPow3"),
    FOUR_BUTTON("Btn4"),
    FIVE_BUTTON("Btn5"),
    SIX_BUTTON("Btn6"),
    MULT_BUTTON("BtnMult"),
    TAN_BUTTON("BtnTan"),
    TAN_H_BUTTON("BtnTanH"),
    CSC_BUTTON("BtnCsc"),
    SQRT_BUTTON("BtnSqrt"),
    POW_2_BUTTON("BtnPow2"),
    ONE_BUTTON("Btn1"),
    TWO_BUTTON("BtnTan"),
    THREE_BUTTON("Btn3"),
    MINUS_BUTTON("BtnMinus"),
    NCR_BUTTON("BtnNcR"),
    NPR_BUTTON("BtnNpR"),
    PER_BUTTON("BtnPer"),
    LOG_10_BUTTON("BtnLog10"),
    POW_10_BUTTON("BtnPow10"),
    ZERO_BUTTON("Btn0"),
    SIGN_BUTTON("BtnSign"),
    DOT_BUTTON("BtnDot"),
    PLUS_BUTTON("BtnPlus"),
    CALC_BUTTON("BtnCalc"),
    TRIGODEG("trigodeg"),
    TRIGORAD("trigorad"),
    OPEN_HISTORY_BUTTON("hist"),
    HISTORY_FRAME("histframe");

    private String id;

    ButtonsEnum(String id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return id;
    }
}
