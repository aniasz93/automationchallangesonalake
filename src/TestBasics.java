import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TestBasics
{
    protected static WebDriver driver;

    @Before
    public void setUp()
    {
        String browser = "ff";

        switch (browser)
        {
            case "ff":
                System.setProperty("webdriver.gecko.driver", ".\\files\\geckodriver-v0.19.1-win64\\geckodriver.exe");
                driver = new FirefoxDriver();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver", ".\\files\\chromedriver_win32\\chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                break;
        }
    }

    @After
    public void cleanUp()
    {
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
