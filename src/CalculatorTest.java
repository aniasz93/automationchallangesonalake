import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest extends TestBasics
{
    private final String EXPECTED_VALUE_FIRST   = "34990";
    private final String EXPECTED_VALUE_SECOND  = "-1";
    private final String EXPECTED_VALUE_THIRD   = "9";
    private final int EXPECTED_VALUE_FOUR       = 3;

    @Test
    public void calculator_test_buttons_clicking()
    {
        try {
            // step 1. Go the page http://web2.0calc.com/
            driver.get("http://web2.0calc.com/");
            CalculatorPage calculatorPage = new CalculatorPage(driver);

            // step 2. Calculate 35*999+(100/4)= and assert the correct result 34990.
            calculatorPage.pressButton(ButtonsEnum.THREE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.FIVE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.MULT_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.NINE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.NINE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.NINE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.PLUS_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.PARAN_L_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.ONE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.ZERO_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.ZERO_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.DIV_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.FOUR_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.PARAN_R_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.CALC_BUTTON);

            Thread.sleep(1000);
            Assert.assertEquals(EXPECTED_VALUE_FIRST, calculatorPage.getResult());

            calculatorPage.pressButton(ButtonsEnum.CLEAR_BUTTON);

            // step 3. Calculate cos(pi) with the rad radio button and assert the correct result -1.
            calculatorPage.pressButton(ButtonsEnum.TRIGORAD);
            calculatorPage.pressButton(ButtonsEnum.PI_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.COS_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.CALC_BUTTON);

            Thread.sleep(1000);
            Assert.assertEquals(EXPECTED_VALUE_SECOND, calculatorPage.getResult());

            calculatorPage.pressButton(ButtonsEnum.CLEAR_BUTTON);

            //step 4. Calculate sqrt(81) and assert assert the correct result 9.
            calculatorPage.pressButton(ButtonsEnum.EIGHT_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.ONE_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.SQRT_BUTTON);
            calculatorPage.pressButton(ButtonsEnum.CALC_BUTTON);

            Thread.sleep(1000);
            Assert.assertEquals(EXPECTED_VALUE_THIRD, calculatorPage.getResult());

            //step 5. Press history dropdown and assert that the list contsains the 3 operations executed e.g. 35*999+(100/4)=, cos(pi),sqrt(81)
            calculatorPage.pressButton(ButtonsEnum.OPEN_HISTORY_BUTTON);
            Assert.assertEquals(EXPECTED_VALUE_FOUR, calculatorPage.getHistoryFrameCount());
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void calculator_test_sending_values()
    {
        try {
            // step 1. Go the page http://web2.0calc.com/
            driver.get("http://web2.0calc.com/");
            CalculatorPage calculatorPage = new CalculatorPage(driver);

            // step 2. Calculate 35*999+(100/4)= and assert the correct result 34990.
            calculatorPage.getInputField().sendKeys("35*999+(100/4)");
            calculatorPage.pressButton(ButtonsEnum.CALC_BUTTON);

            Thread.sleep(1000);
            Assert.assertEquals(EXPECTED_VALUE_FIRST, calculatorPage.getResult());

            calculatorPage.pressButton(ButtonsEnum.CLEAR_BUTTON);

            // step 3. Calculate cos(pi) with the rad radio button and assert the correct result -1.
            calculatorPage.pressButton(ButtonsEnum.TRIGORAD);
            calculatorPage.getInputField().sendKeys("cos(pi)");
            calculatorPage.pressButton(ButtonsEnum.CALC_BUTTON);

            Thread.sleep(1000);
            Assert.assertEquals(EXPECTED_VALUE_SECOND, calculatorPage.getResult());

            calculatorPage.pressButton(ButtonsEnum.CLEAR_BUTTON);

            // step 4. Calculate sqrt(81) and assert assert the correct result 9.
            calculatorPage.getInputField().sendKeys("sqrt(81)");
            calculatorPage.pressButton(ButtonsEnum.CALC_BUTTON);

            Thread.sleep(1000);
            Assert.assertEquals(EXPECTED_VALUE_THIRD, calculatorPage.getResult());

            calculatorPage.pressButton(ButtonsEnum.CLEAR_BUTTON);

            // step 5. Press history dropdown and assert that the list contains the 3 operations executed e.g. 35*999+(100/4)=, cos(pi),sqrt(81)
            calculatorPage.pressButton(ButtonsEnum.OPEN_HISTORY_BUTTON);
            Assert.assertEquals(EXPECTED_VALUE_FOUR, calculatorPage.getHistoryFrameCount());
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
