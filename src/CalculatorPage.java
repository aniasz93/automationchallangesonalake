import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CalculatorPage extends PageObject
{
    @FindBy(id="input")
    private WebElement inputField;

    @FindBy(id="Btn2nd")
    private WebElement twoNdButton;

    @FindBy(id="BtnPi")
    private WebElement piButton;

    @FindBy(id="BtnE")
    private WebElement eButton;

    @FindBy(id="BtnMatrix")
    private WebElement matrixButton;

    @FindBy(id="BtnVar")
    private WebElement varButton;

    @FindBy(id="BtnParanL")
    private WebElement paranLButton;

    @FindBy(id="BtnColon")
    private WebElement colonButton;

    @FindBy(id="BtnParanR")
    private WebElement paranRButton;

    @FindBy(id="BtnLeft")
    private WebElement leftButton;

    @FindBy(id="BtnBack")
    private WebElement backButton;

    @FindBy(id="BtnSin")
    private WebElement sinButton;

    @FindBy(id="BtnSinH")
    private WebElement sinHButton;

    @FindBy(id="BtnCot")
    private WebElement cotButton;

    @FindBy(id="BtnSqrtY")
    private WebElement sqrtYButton;

    @FindBy(id="BtnPowXY")
    private WebElement powXYButton;

    @FindBy(id="Btn7")
    private WebElement sevenButton;

    @FindBy(id="Btn8")
    private WebElement eightButton;

    @FindBy(id="Btn9")
    private WebElement nineButton;

    @FindBy(id="BtnDiv")
    private WebElement divButton;

    @FindBy(id="BtnClear")
    private WebElement clearButton;

    @FindBy(id="BtnCos")
    private WebElement cosButton;

    @FindBy(id="BtnCosH")
    private WebElement cosHButton;

    @FindBy(id="BtnSec")
    private WebElement secButton;

    @FindBy(id="BtnSqrt3")
    private WebElement sqrt3Button;

    @FindBy(id="BtnPow3")
    private WebElement pow3Button;

    @FindBy(id="Btn4")
    private WebElement fourButton;

    @FindBy(id="Btn5")
    private WebElement fiveButton;

    @FindBy(id="Btn6")
    private WebElement sixButton;

    @FindBy(id="BtnMult")
    private WebElement multButton;

    @FindBy(id="BtnTan")
    private WebElement tanButton;

    @FindBy(id="BtnTanH")
    private WebElement tanHButton;

    @FindBy(id="BtnCsc")
    private WebElement cscButton;

    @FindBy(id="BtnSqrt")
    private WebElement sqrtButton;

    @FindBy(id="BtnPow2")
    private WebElement pow2Button;

    @FindBy(id="Btn1")
    private WebElement oneButton;

    @FindBy(id="Btn2")
    private WebElement twoButton;

    @FindBy(id="Btn3")
    private WebElement threeButton;

    @FindBy(id="BtnMinus")
    private WebElement minusButton;

    @FindBy(id="BtnNcR")
    private WebElement ncRButton;

    @FindBy(id="BtnNpR")
    private WebElement npRButton;

    @FindBy(id="BtnPer")
    private WebElement perButton;

    @FindBy(id="BtnLog10")
    private WebElement log10Button;

    @FindBy(id="BtnPow10")
    private WebElement pow10Button;

    @FindBy(id="Btn0")
    private WebElement zeroButton;

    @FindBy(id="BtnSign")
    private WebElement signButton;

    @FindBy(id="BtnDot")
    private WebElement dotButton;

    @FindBy(id="BtnPlus")
    private WebElement plusButton;

    @FindBy(id="BtnCalc")
    private WebElement calcButton;

    @FindBy(id="trigodeg")
    private WebElement trigodeg;

    @FindBy(id="trigorad")
    private WebElement trigorad;

    @FindBy(id="hist")
    private WebElement openHistoryButton;

    @FindBy(id="histframe")
    private WebElement historyFrame;

    public CalculatorPage(WebDriver driver)
    {
        super(driver);
    }

    public void pressButton(ButtonsEnum buttonStr)
    {
        switch (buttonStr)
        {
            case INPUT_FIELD:
                inputField.click();
                break;
            case TWO_ND_BUTTON:
                twoNdButton.click();
                break;
            case PI_BUTTON:
                piButton.click();
                break;
            case E_BUTTON:
                eButton.click();
                break;
            case MATRIX_BUTTON:
                matrixButton.click();
            case VAR_BUTTON:
                varButton.click();
                break;
            case PARAN_L_BUTTON:
                paranLButton.click();
                break;
            case COLON_BUTTON:
                colonButton.click();
                break;
            case PARAN_R_BUTTON:
                paranRButton.click();
                break;
            case LEFT_BUTTON:
                leftButton.click();
                break;
            case BACK_BUTTON:
                backButton.click();
                break;
            case SIN_BUTTON:
                sinButton.click();
                break;
            case SIN_H_BUTTON:
                sinHButton.click();
                break;
            case COT_BUTTON:
                cotButton.click();
                break;
            case SQRT_Y_BUTTON:
                sqrtYButton.click();
                break;
            case POW_XY_BUTTON:
                powXYButton.click();
                break;
            case SEVEN_BUTTON:
                sevenButton.click();
                break;
            case EIGHT_BUTTON:
                eightButton.click();
                break;
            case NINE_BUTTON:
                nineButton.click();
                break;
            case DIV_BUTTON:
                divButton.click();
                break;
            case CLEAR_BUTTON:
                clearButton.click();
                break;
            case COS_BUTTON:
                cosButton.click();
                break;
            case COS_H_BUTTON:
                cosHButton.click();
                break;
            case SEC_BUTTON:
                secButton.click();
                break;
            case SQRT_3_BUTTON:
                sqrt3Button.click();
                break;
            case POW_3_BUTTON:
                pow3Button.click();
                break;
            case FOUR_BUTTON:
                fourButton.click();
                break;
            case FIVE_BUTTON:
                fiveButton.click();
                break;
            case SIX_BUTTON:
                sixButton.click();
                break;
            case MULT_BUTTON:
                multButton.click();
                break;
            case TAN_BUTTON:
                tanButton.click();
                break;
            case TAN_H_BUTTON:
                tanHButton.click();
                break;
            case CSC_BUTTON:
                cscButton.click();
                break;
            case SQRT_BUTTON:
                sqrtButton.click();
                break;
            case POW_2_BUTTON:
                pow2Button.click();
                break;
            case ONE_BUTTON:
                oneButton.click();
                break;
            case TWO_BUTTON:
                twoNdButton.click();
                break;
            case THREE_BUTTON:
                threeButton.click();
                break;
            case MINUS_BUTTON:
                minusButton.click();
                break;
            case NCR_BUTTON:
                ncRButton.click();
                break;
            case NPR_BUTTON:
                npRButton.click();
                break;
            case PER_BUTTON:
                perButton.click();
                break;
            case LOG_10_BUTTON:
                log10Button.click();
                break;
            case POW_10_BUTTON:
                pow10Button.click();
                break;
            case ZERO_BUTTON:
                zeroButton.click();
                break;
            case SIGN_BUTTON:
                signButton.click();
                break;
            case DOT_BUTTON:
                dotButton.click();
                return;
            case PLUS_BUTTON:
                plusButton.click();
                break;
            case CALC_BUTTON:
                calcButton.click();
                break;
            case TRIGODEG:
                trigodeg.click();
                break;
            case TRIGORAD:
                trigorad.click();
                break;
            case OPEN_HISTORY_BUTTON:
                openHistoryButton.click();
                break;
            case HISTORY_FRAME:
                historyFrame.click();
                break;
            default:
                clearButton.click();
        }
    }

    public int getHistoryFrameCount()
    {
        String historyFrameValuesXPATH = ".//div[@id='histframe']/ul/li";
        List<WebElement> historyValues = driver.findElements(By.xpath(historyFrameValuesXPATH));

        return historyValues.size();
    }

    public WebElement getInputField()
    {
        return inputField;
    }

    public String getResult()
    {
        return inputField.getAttribute("value");
    }
}
